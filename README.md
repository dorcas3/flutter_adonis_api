# LIST OF INTERNS

A Flutter app that is used to display a list of interns.

## App Images

![Login](assets/images/logininterns.png) ![Signup](assets/images/signupinterns.png) ![Home](assets/images/home.png)
## User requirements

- The application should have a login page that once someone logs in, they get redirected to a page of the interns
- This should be filterable by cohort year and month
## Endpoints to consume

- [InternsEndpoints](https://documenter.getpostman.com/view/10658641/TzRRBnt5)

## Prerequisites
- Flutter v2.2.0

## Setup and Installations

- Clone the repository into your local machine
- In the terminal type flutter doctor to install all the dependencies
- Type flutter run command to launch the application

### License

* LICENSED UNDER  [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](license/MIT)
