import 'package:flutter/material.dart';
import 'package:interns_crud/widgets/login.dart';
import 'package:interns_crud/widgets/home.dart';
import 'package:interns_crud/widgets/signup.dart';
import 'package:provider/provider.dart';
import 'package:interns_crud/providers/login_provider.dart';
import 'package:interns_crud/providers/signup_provider.dart';
import 'package:interns_crud/providers/interns_provider.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => LoginProvider(),
        ),
          ChangeNotifierProvider(
          create: (ctx) => SignupProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => InternProvider(),
        ),
        
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (context) => Login(),
          '/signup': (context) => Signup(),
          '/home': (context) => Home(),
        },
      ),
    );
  }
}


