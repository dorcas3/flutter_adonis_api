
import 'package:flutter/material.dart';
import 'package:interns_crud/models/User.dart';
import 'package:interns_crud/services/login_service.dart';

class LoginProvider with ChangeNotifier{

User _user;
User get user => _user;

Future<void> getLoginCredentials(String email, String password) async {
    try {
      _user = (await loginUser(email,password));
      print(_user);
      notifyListeners();
    } catch (e) {
      print('errors');
      notifyListeners();
    }
  }
}