
import 'package:flutter/material.dart';
import 'package:interns_crud/models/UserRegister.dart';
import 'package:interns_crud/services/register_service.dart';


class SignupProvider with ChangeNotifier{


UserRegister _userregister;

UserRegister get userregister => _userregister;

Future<void> getRegisteredUser(String email, String password) async {
    try {
      _userregister = (await registerUser(email,password));
      print(_userregister);
      notifyListeners();
    } catch (e) {
      print('errors');
      notifyListeners();
    }
  }
}