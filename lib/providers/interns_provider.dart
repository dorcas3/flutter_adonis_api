
import 'package:flutter/material.dart';
import 'package:interns_crud/models/Intern.dart';
import 'package:interns_crud/services/interns_service.dart';

class InternProvider with ChangeNotifier{
List<Intern> _interns = [];
// Intern _intern;
// Intern get intern => _intern;
List<Intern> get interns => _interns;

Future<void> getInternsDetails(String year, String month) async {
    try {
      _interns = (await fetchInterns(year,month)).interns;
       print(_interns);
      notifyListeners();
    } catch (e) {
      print('errors');
      notifyListeners();
    }
  }
}