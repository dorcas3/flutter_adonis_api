class InternList {

  List<Intern> interns = [];

  InternList(this.interns);

  InternList.fromJson(Map<String, dynamic> json) {
  
      json['interns'].forEach((intern) {
        interns.add(new Intern.fromJson(intern));
      });
 
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if(this.interns != null){
      data['interns'] = this.interns.map((intern) => intern.toJson()).toList();
      // print(data);
    }
      
    return data;
  }
}

class Intern {

  String username;
  String email;
  String month;
  int  year;

 

  Intern({this.username,this.email,this.month,this.year});

  Intern.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    email = json['email'];
    month = json['month'];
    year = json['year'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['email'] = this.username;
    data['month'] = this.username;
    data['year'] = this.year;
  
    return data;
  }
}