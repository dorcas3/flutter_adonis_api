class UserRegister{

  String email;
  String password;
  
  // ignore: non_constant_identifier_names
  UserRegister userregister;

  UserRegister({this.email,this.password});

  UserRegister.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;

  
    return data;
  }
}