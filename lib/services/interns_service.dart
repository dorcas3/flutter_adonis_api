import 'dart:convert';
import 'package:http/http.dart';
import 'package:interns_crud/models/Intern.dart';






Future<InternList> fetchInterns(String year, String month) async {
  final response = Uri.parse('https://c01d60e7349ec8.localhost.run/interns/year-month/$year/$month');

//  'https://618045b3d0a9f6.localhost.run/interns-filter?year=$year&month=$month'
  final res = await get(response);
  // print(res.body);
  return (InternList.fromJson(json.decode(res.body)));
}
