import 'dart:convert';

import 'package:http/http.dart';

import 'package:interns_crud/models/UserRegister.dart';


  Future <UserRegister>registerUser(String email,String password) async {

    try{
    final response = Uri.parse(
        'https://b9e764fad3badf.localhost.run/register/');
    // print(response);
       
    var data = {"email": email, "password": password};
    // print(data);
    final res = await post(response,body: data);
    print(res.body);

    if (res.statusCode == 200) {
      
      return UserRegister.fromJson(jsonDecode(res.body));
    } else {
      throw Exception('Failed to register user');
    }
    }catch(e){
      throw 'Error';
    }

  }

  