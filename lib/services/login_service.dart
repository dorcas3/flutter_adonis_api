
import 'dart:convert';

import 'package:http/http.dart';

import 'package:interns_crud/models/User.dart';


  Future <User>loginUser(String email,String password) async {

    try{
    final response = Uri.parse(
        'https://b9e764fad3badf.localhost.run/login/');
    // print(response);
       
    var data = {"email": email, "password": password};
    // print(data);
    final res = await post(response,body: data);
    print(res.body);

    if (res.statusCode == 200) {
      
      return User.fromJson(jsonDecode(res.body));
    } else {
      throw Exception('Failed to load user');
    }
    }catch(e){
      throw 'Error';
    }

  }

  