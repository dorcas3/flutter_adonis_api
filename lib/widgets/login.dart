import 'package:flutter/material.dart';
import 'package:interns_crud/providers/login_provider.dart';
import 'package:interns_crud/widgets/signup.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

var email, password;

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        toolbarHeight: 100.0,
        backgroundColor: Colors.green,
        title: Text(
          'INTERNS',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(30.0, 40.0, 40.0, 0.0),
        child: Form(
          child: Column(
            children: [
              // Image.asset('assets/images/logo.png'),
              Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 50.0),
                child: Text(
                  'LOGIN',
                  style: TextStyle(color: Colors.amber, fontSize: 30.0),
                ),
              ),
              TextFormField(
                  decoration: InputDecoration(
                      icon: Icon(
                        Icons.person,
                        color: Colors.amber,
                      ),
                      hintText: 'Enter Your Email Address'),
                  onChanged: (val) {
                    setState(() {
                      email = val;
                      // print(email);
                    });
                  }),
              SizedBox(
                height: 40.0,
              ),
              TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                      icon: Icon(
                        Icons.lock,
                        color: Colors.green,
                      ),
                      hintText: 'Enter Your Password'),
                  onChanged: (val) {
                    setState(() {
                      password = val;
                      // print(password);
                    });
                  }),
              SizedBox(
                height: 40.0,
              ),

              Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(20)),
                child: ElevatedButton(
                  child: Text(
                    'LOGIN',
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.w300),
                  ),
                  onPressed: () {
                    var result = context
                        .read<LoginProvider>()
                        .getLoginCredentials(email, password);
                    if (result == null) {
                    } else {
                      Navigator.pushNamed(context, '/home');
                    }
                  },
                ),
              ),
              SizedBox(
                height: 130,
              ),
              InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, '/signup');
                  },
                  child: Text('New User? Create Account',
                      style: TextStyle(color: Colors.blue, fontSize: 12.0))),
            ],
          ),
        ),
      ),
    );
  }
}
