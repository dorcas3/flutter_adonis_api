import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:interns_crud/providers/interns_provider.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

var year;
var month;

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    var state = context.watch<InternProvider>();
    return Container(
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 100.0,
          backgroundColor: Colors.green,
          title: Text(
            'INTERNS',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 50.0),
                child: Text(
                  'LIST OF INTERNS',
                  style: TextStyle(color: Colors.amber, fontSize: 30.0),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new Flexible(
                        child: DropdownButtonFormField(
                            decoration: InputDecoration(
                              labelText: 'Select year',
                            ),
                            value: year,
                            items: [
                              '2001',
                              '2003',
                              '2004',
                              '2005',
                              '2006',
                              '2007',
                              '2008',
                              '2009',
                              '2010',
                              '2011',
                              '2012',
                              '2013',
                              '2014',
                              '2015',
                              '2016',
                              '2017',
                              '2018',
                              '2019',
                              '2020'
                            ]
                                .map((data) => DropdownMenuItem(
                                      child: Text(data),
                                      value: data,
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                year = value;
                                print(year);
                              });
                            })),
                    SizedBox(
                      width: 20.0,
                    ),
                    new Flexible(
                        child: DropdownButtonFormField(
                            decoration: InputDecoration(
                              labelText: 'Select month',
                            ),
                            value: month,
                            items: [
                              'January',
                              'February',
                              'March',
                              'April',
                              'may',
                              'June',
                              'July',
                              'August',
                              'September',
                              'October',
                              'November',
                              'December'
                            ]
                                .map((data) => DropdownMenuItem(
                                      child: Text(data),
                                      value: data,
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                month = value;
                                print(month);
                              });
                            }))
                  ],
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              Container(
                constraints: BoxConstraints(maxWidth: 250.0, minHeight: 50.0),
                child: ElevatedButton(
                  child: Text(
                    'CHECK INTERNS',
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.w300),
                  ),
                  
                  onPressed: () {
                    context
                        .read<InternProvider>()
                        .getInternsDetails(year, month);
                  },
                ),
              ),

               
                SizedBox(
                height: 20.0,
              ),
              ListView.builder(
                  itemCount: state.interns.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return InkWell(
                      child: Card(
                        elevation: 6.0,
                        child: Container(
                          padding: EdgeInsets.all(8.0),
                          margin: EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Card(
                                child: Column(
                                  children: [
                                    ListTile(
                                      leading: Icon(Icons.person,color: Colors.green,),
                                      title: Text(
                                          "${state.interns[index].username}"),
                                    ),
                                    ListTile(
                                      leading: Icon(
                                        Icons.email,
                                        color: Colors.amber,
                                      ),
                                      title:
                                          Text("${state.interns[index].email}"),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }
}
